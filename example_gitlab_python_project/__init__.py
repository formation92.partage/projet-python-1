"""Top-level package for Example GitLab Python Project."""

__author__ = """Adriaan Rol"""
__email__ = 'adriaan.rol@example.com'
__version__ = '0.1.0'
